const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    mongoConfig: {
        db: 'mongodb://localhost/gallery',
        options: {useNewUrlParser: true},
    },
    facebook: {
        appId: '696817958179442',
        appSecret: '003a107fc3f758e2c27701889ba541ef'
    }
}
