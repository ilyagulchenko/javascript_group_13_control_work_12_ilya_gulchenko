const mongoose = require('mongoose');
const config = require('./config');
const Photo = require("./models/Photo");
const User = require("./models/User");
const { nanoid } = require("nanoid");

const run = async () => {
    await mongoose.connect(config.mongoConfig.db, config.mongoConfig.options);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [userOne, userTwo] = await User.create({
        displayName: 'Jon Doe',
        token: nanoid(),
        role: 'user',
        avatar: 'userOne.png'
    }, {
        displayName: 'Will Smith',
        token: nanoid(),
        role: 'user',
        avatar: 'userTwo.jpg'
    });

    await Photo.create({
            user: userOne,
            title: 'Bull',
            image: 'picture.png',
        },
        {
            user: userOne,
            title: 'Nature',
            image: 'picture1.jpeg',
        },
        {
            user: userOne,
            title: 'Football player',
            image: 'picture2.jpg',
        },
        {
            user: userTwo,
            title: 'Football player',
            image: 'picture3.jpg',
        },
        {
            user: userTwo,
            title: 'Car',
            image: 'picture4.jpg',
        },
        {
            user: userTwo,
            title: 'Fast car',
            image: 'picture5.png',
        });

    await mongoose.connection.close();
};

run().catch(e => console.error(e));
