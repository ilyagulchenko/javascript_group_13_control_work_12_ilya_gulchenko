const express = require('express');
const multer = require("multer");
const config = require("../config");
const { nanoid } = require("nanoid");
const path = require("path");
const Photo = require('../models/Photo');
const auth = require("../middleware/auth");
const User = require("../models/User");

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get("/", async (req, res, next) => {
    try {
        const query = {};

        if (req.query.user) {
            query.user = req.query.user;

            const data = {
                photos: await Photo.find(query).populate("user", "displayName"),
                user: await User.findById(req.query.user)
            }

            return res.send(data);
        }

        const photos = await Photo.find().populate('user', 'displayName');

        return res.send(photos);
    } catch (e) {
        next(e);
    }
});

router.get("/:id", async (req, res, next) => {
    try {
        const photo = await Photo.findById(req.params.id);

        if (!photo) {
            return res.status(404).send({message: 'Not found!'});
        }

        return res.send(photo);
    } catch (e) {
        next(e);
    }
});

router.post("/", auth, upload.single('image'), async (req, res, next) => {
    try {
        if (!req.file) {
            return res.status(404).send({message: 'Image is required!'});
        }

        if (!req.body.title) {
            return res.status(404).send({message: 'Title is required!'});
        }

        const photoData = {
            user: req.user,
            title: req.body.title,
            image: req.file.filename,
        };

        const photo = new Photo(photoData);

        await photo.save();

        return res.send(photo);
    } catch (e) {
        next(e);
    }
});

router.delete("/:id", auth, async (req, res, next) => {
    try {
        await Photo.deleteOne({_id: req.params.id});

        return res.send({message: 'Photo was deleted'});
    } catch (e) {
        next(e);
    }
});

module.exports = router;
