import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginFacebookUser, LoginUserData, RegisterUserData, User } from '../models/user.model';
import { environment as env } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersServices {
  constructor(private http: HttpClient) {}

  registerUser(userdata: RegisterUserData) {
    return this.http.post<User>(env.apiUrl + '/users', userdata);
  }

  login(userData: LoginUserData) {
    return this.http.post<User>(env.apiUrl + '/users/sessions', userData);
  }

  logout() {
    return this.http.delete(env.apiUrl + '/users/sessions');
  }

  facebookLogin(userData: LoginFacebookUser) {
    return this.http.post<User>(env.apiUrl + '/users/facebookLogin', userData);
  }
}
