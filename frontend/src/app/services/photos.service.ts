import { HttpClient } from '@angular/common/http';
import { DataApi, Photo, PhotoData } from '../models/photo.model';
import { environment as env } from '../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PhotosService {
  constructor(private http: HttpClient) {}

  getPhotos() {
    return this.http.get<Photo[]>(env.apiUrl + '/photos');
  }

  getPhotosOfUser(id: string) {
    return this.http.get<DataApi>(env.apiUrl + `/photos?user=${id}`);
  }

  getPhotoById(id: string) {
    return this.http.get<Photo>(env.apiUrl + '/photos/' + id);
  }

  createPhoto(photoData: PhotoData) {
    const formData = new FormData();

    Object.keys(photoData).forEach(key => {
      if (photoData[key] !== null) {
        formData.append(key, photoData[key]);
      }
    });

    return this.http.post(env.apiUrl + '/photos', formData);
  }

  removePhoto(id: string) {
    return this.http.delete(env.apiUrl + `/photos/${id}`);
  }
}
