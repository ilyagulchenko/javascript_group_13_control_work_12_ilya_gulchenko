import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';
import { localStorageSync } from 'ngrx-store-localstorage';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { usersReducer } from './users/users.reducer';
import { UsersEffects } from './users/users.effects';
import { photosReducer } from './photos/photos.reducer';
import { PhotosEffects } from './photos/photos.effects';
import { dataReducer } from './data/data.reducer';
import { DataEffects } from './data/data.effects';

const localStorageSyncReducer = (reducer: ActionReducer<any>) => {
  return localStorageSync({
    keys: [{users: ['user']}],
    rehydrate: true
  })(reducer);
}

const metaReducers: MetaReducer[] = [localStorageSyncReducer];

const reducers = {
  users: usersReducer,
  photos: photosReducer,
  data: dataReducer,
}

const effects = [UsersEffects, PhotosEffects, DataEffects]

@NgModule({
  imports: [
    StoreModule.forRoot(reducers, {metaReducers}),
    EffectsModule.forRoot(effects),
  ]
})
export class AppStoreModule {}

