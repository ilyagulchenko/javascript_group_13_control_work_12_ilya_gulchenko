import { LoginError, RegisterError, User } from '../models/user.model';
import { DataApi, Photo } from '../models/photo.model';

export type UsersState = {
  user: null | User,
  loginLoading: boolean,
  loginError: null | LoginError,
  registerLoading: boolean,
  registerError: null | RegisterError,
};

export type PhotosState = {
  photos: Photo[],
  photo: null | Photo,
  fetchLoading: boolean,
  fetchError: null | string,
  getLoading: boolean,
  getError: null | string,
  createLoading: boolean,
  createError: null | string,
  removeLoading: boolean,
  removeError: null | string,
};

export type DataState = {
  data: null | DataApi,
  fetchLoading: boolean
}


export type AppState = {
  users: UsersState,
  photos: PhotosState,
  data: DataState,
};
