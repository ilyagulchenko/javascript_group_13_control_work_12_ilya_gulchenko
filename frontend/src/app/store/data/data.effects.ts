import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { fetchDataRequest, fetchDataSuccess } from './data.actions';
import { map, mergeMap } from 'rxjs';
import { PhotosService } from '../../services/photos.service';

@Injectable()
export class DataEffects {
  fetchData = createEffect(() => this.actions.pipe(
    ofType(fetchDataRequest),
    mergeMap(({id}) => this.photosService.getPhotosOfUser(id).pipe(
      map(data => fetchDataSuccess({data}))
    ))
  ));

  constructor(
    private actions: Actions,
    private photosService: PhotosService,
  ) {}
}
