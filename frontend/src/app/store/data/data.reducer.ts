import { DataState } from '../types';
import { createReducer, on } from '@ngrx/store';
import { fetchDataRequest, fetchDataSuccess } from './data.actions';

const initialState: DataState = {
  data: null,
  fetchLoading: false,
}

export const dataReducer = createReducer(
  initialState,
  on(fetchDataRequest, state => ({...state, fetchLoading: true})),
  on(fetchDataSuccess, (state, {data}) => ({...state, fetchLoading: false, data})),
);
