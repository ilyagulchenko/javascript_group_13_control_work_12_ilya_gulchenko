import { PhotosState } from '../types';
import { createReducer, on } from '@ngrx/store';
import {
  createPhotoFailure,
  createPhotoRequest, createPhotoSuccess,
  fetchPhotosFailure,
  fetchPhotosRequest,
  fetchPhotosSuccess,
  getPhotoFailure,
  getPhotoRequest,
  getPhotoSuccess, removePhotoFailure, removePhotoRequest, removePhotoSuccess
} from './photos.actions';

const initialState: PhotosState = {
  photos: [],
  photo: null,
  fetchLoading: false,
  fetchError: null,
  getLoading: false,
  getError: null,
  createLoading: false,
  createError: null,
  removeLoading: false,
  removeError: null,
}

export const photosReducer = createReducer(
  initialState,
  on(fetchPhotosRequest, state => ({...state, fetchLoading: true})),
  on(fetchPhotosSuccess, (state, {photos}) => ({...state, fetchLoading: false, photos})),
  on(fetchPhotosFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(getPhotoRequest, state => ({...state, getLoading: true})),
  on(getPhotoSuccess, (state, {photo}) => ({...state, fetchLoading: false, photo})),
  on(getPhotoFailure, (state, {error}) => ({...state, fetchLoading: false, getError: error})),

  on(createPhotoRequest, state => ({...state, createLoading: true})),
  on(createPhotoSuccess, state => ({...state, createLoading: false})),
  on(createPhotoFailure, (state, {error}) => ({...state, createLoading: false, createError: error})),

  on(removePhotoRequest, state => ({...state, removeLoading: true})),
  on(removePhotoSuccess, state => ({...state, removeLoading: false})),
  on(removePhotoFailure, (state, {error}) => ({...state, removeLoading: false, removeError: error})),
);
