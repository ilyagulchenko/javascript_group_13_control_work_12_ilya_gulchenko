import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { PhotosService } from '../../services/photos.service';
import { catchError, map, mergeMap, of, tap } from 'rxjs';
import {
  createPhotoFailure,
  createPhotoRequest, createPhotoSuccess,
  fetchPhotosFailure,
  fetchPhotosRequest,
  fetchPhotosSuccess,
  getPhotoRequest,
  getPhotoSuccess,
  removePhotoFailure,
  removePhotoRequest,
  removePhotoSuccess
} from './photos.actions';
import { HelpersService } from '../../services/helpers.service';
import { Router } from '@angular/router';

@Injectable()
export class PhotosEffects {
  constructor(
    private actions: Actions,
    private photosService: PhotosService,
    private helpers: HelpersService,
    private router: Router
  ) {}

  fetchPhotos = createEffect(() => this.actions.pipe(
    ofType(fetchPhotosRequest),
    mergeMap(() => this.photosService.getPhotos().pipe(
      map( photos => fetchPhotosSuccess({photos})),
      catchError(() => of(fetchPhotosFailure({error: 'Something wrong'})))
    ))
  ));

  getPhoto = createEffect(() => this.actions.pipe(
    ofType(getPhotoRequest),
    mergeMap(({id}) => this.photosService.getPhotoById(id).pipe(
      map(photo => getPhotoSuccess({photo}))
    ))
  ));

  createPhoto = createEffect(() => this.actions.pipe(
    ofType(createPhotoRequest),
    mergeMap(({photoData}) => this.photosService.createPhoto(photoData).pipe(
      map(() => createPhotoSuccess()),
      tap(() => {this.helpers.openSnackbar('Photo added');
        void this.router.navigate(['/']);
      }),
      catchError(() => of(createPhotoFailure({error: 'Wrong data'})))
    ))
  ));

  removePhoto = createEffect(() => this.actions.pipe(
    ofType(removePhotoRequest),
    mergeMap(({id}) => this.photosService.removePhoto(id).pipe(
      map(() => removePhotoSuccess()),
      catchError(() => of(removePhotoFailure({error: 'Something wrong'})))
    ))
  ));
}
