import { createAction, props } from '@ngrx/store';
import { Photo, PhotoData } from '../../models/photo.model';

export const fetchPhotosRequest = createAction('[Photos] Fetch Request');
export const fetchPhotosSuccess = createAction('[Photos] Fetch Success', props<{photos: Photo[]}>());
export const fetchPhotosFailure = createAction('[Photos] Fetch Failure', props<{error: string}>());

export const getPhotoRequest = createAction('[Photo] Get Request', props<{id: string}>());
export const getPhotoSuccess = createAction('[Photo] Get Success', props<{photo: Photo}>());
export const getPhotoFailure = createAction('[Photo] Get Failure', props<{error: string}>());

export const createPhotoRequest = createAction('[Photo] Create Request', props<{photoData: PhotoData}>());
export const createPhotoSuccess = createAction('[Photo] Create Success');
export const createPhotoFailure = createAction('[Photo] Create Failure', props<{error: string}>());

export const removePhotoRequest = createAction('[Photo] Remove Request', props<{id: string}>());
export const removePhotoSuccess = createAction('[Photo] Remove Success');
export const removePhotoFailure = createAction('[Photo] Remove Failure', props<{error: string}>());
