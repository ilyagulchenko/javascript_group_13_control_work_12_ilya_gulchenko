import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { PhotosComponent } from './pages/photos/photos.component';
import { UserPhotosComponent } from './pages/photos/user-photos/user-photos.component';
import { EditPhotoComponent } from './pages/photos/edit-photo/edit-photo.component';
import { RegisterComponent } from './pages/register/register.component';

const routes: Routes = [
  {path: '', component: PhotosComponent},
  {path: 'photos/new', component: EditPhotoComponent},
  {path: 'photos/:id', component: UserPhotosComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
