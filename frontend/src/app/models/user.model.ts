export interface User {
  _id: string,
  displayName: string,
  token: string,
  role: string,
  avatar: string,
}

export interface LoginFacebookUser {
  id: string,
  authToken: string,
  email: string,
  name: string,
  avatar: string
}

export interface LoginUserData {
  displayName: string,
}

export interface RegisterUserData {
  displayName: string,
}

export interface LoginError {
  error: string,
}

export interface RegisterError {
  errors: {
    displayName: FieldError,
  }
}

export interface FieldError {
  message: string,
}
