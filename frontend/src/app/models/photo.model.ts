export interface Photo {
  _id: string,
  user: {
    _id: string,
    displayName: string
  },
  title: string,
  image: string,
}

export interface DataApi {
  photos: Photo[],
  user: {
    _id: string,
    displayName: string
  }
}

export interface PhotoData {
  [key: string]: any;
  user: string;
  title: string;
  image: File;
}
