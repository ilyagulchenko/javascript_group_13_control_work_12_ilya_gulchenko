import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/types';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../../models/user.model';
import { fetchDataRequest } from '../../../store/data/data.actions';
import { DataApi } from '../../../models/photo.model';
import { getPhotoRequest, removePhotoRequest } from '../../../store/photos/photos.actions';
import { MatDialog } from '@angular/material/dialog';
import { ModalWindowComponent } from '../../../modal-window/modal-window.component';

@Component({
  selector: 'app-user-photos',
  templateUrl: './user-photos.component.html',
  styleUrls: ['./user-photos.component.sass']
})
export class UserPhotosComponent implements OnInit {
  data: Observable<null | DataApi>;
  user: Observable<null | User>;
  id = '';

  constructor(
    private store: Store<AppState>,
    private route: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog
    ) {
    this.data = store.select(state => state.data.data);
    this.user = store.select(state => state.users.user);
    this.user.subscribe(user => {
      if (user) {
        this.id = user._id;
      }
    });
  }

  ngOnInit(): void {
    // @ts-ignore
    const id: string = this.route.params.value.id
    this.store.dispatch(fetchDataRequest({id}));
  }

  openDialog(id: string) {
    this.dialog.open(ModalWindowComponent);
    this.store.dispatch(getPhotoRequest({id}));
  }

  removePhoto(id: string) {
    this.store.dispatch(removePhotoRequest({id}));
    void this.router.navigate(['/']);
  }

}
