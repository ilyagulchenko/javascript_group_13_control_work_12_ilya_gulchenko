import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/types';
import { PhotoData } from '../../../models/photo.model';
import { createPhotoRequest } from '../../../store/photos/photos.actions';

@Component({
  selector: 'app-edit-photo',
  templateUrl: './edit-photo.component.html',
  styleUrls: ['./edit-photo.component.sass']
})
export class EditPhotoComponent {
  @ViewChild('f') form!: NgForm;

  constructor(private store: Store<AppState>) { }

  createPhoto() {
    const photoData: PhotoData = this.form.value;
    this.store.dispatch(createPhotoRequest({photoData}));
  }

}
