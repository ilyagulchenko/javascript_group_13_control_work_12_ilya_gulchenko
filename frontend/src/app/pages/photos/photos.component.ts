import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Photo } from '../../models/photo.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { fetchPhotosRequest, getPhotoRequest } from '../../store/photos/photos.actions';
import { MatDialog } from '@angular/material/dialog';
import { ModalWindowComponent } from '../../modal-window/modal-window.component';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.sass']
})
export class PhotosComponent implements OnInit {
  photos: Observable<Photo[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;

  constructor(private store: Store<AppState>, private dialog: MatDialog) {
    this.photos = store.select(state => state.photos.photos);
    this.loading = store.select(state => state.photos.fetchLoading);
    this.error = store.select(state => state.photos.fetchError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchPhotosRequest());
  }

  openDialog(id: string) {
    this.dialog.open(ModalWindowComponent);
    this.store.dispatch(getPhotoRequest({id}));
  }

}
