import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Photo } from '../models/photo.model';
import { Store } from '@ngrx/store';
import { AppState } from '../store/types';

@Component({
  selector: 'app-modal-window',
  templateUrl: './modal-window.component.html',
  styleUrls: ['./modal-window.component.sass']
})
export class ModalWindowComponent implements OnInit {
  photo: Observable<null | Photo>;

  constructor(private store: Store<AppState>) {
    this.photo = store.select(state => state.photos.photo);
  }

  ngOnInit(): void {}

}
